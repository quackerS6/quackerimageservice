﻿using System;

namespace ImageService.Logic {
    public class ColorCalculator {

        /// <summary>
        /// Calculates the grayscale for the given color
        /// </summary>
        /// <param name="red">The color's RGB red value</param>
        /// <param name="green">The color's RGB green value</param>
        /// <param name="blue">The color's RGB blue value</param>
        /// <returns>
        /// The grayscale of the color as a percentage
        /// 0% = black 100% white
        /// </returns>
        public double CalcGrayScalePercentage(int red, int green, int blue) {
            // grayScale% = (0.299 R + 0.587 G + 0.114 B) / 255 * 100;
            // Formula from: https://goodcalculators.com/rgb-to-grayscale-conversion-calculator/
            IsValidRgb(new int[] { red, green, blue });
            double grayRed = red * 0.299;
            double grayGreen = green * 0.587;
            double grayBlue = blue * 0.144;
            double rgbSum = grayRed + grayGreen + grayBlue;
            return rgbSum / 2.55;
        }

        /// <param name="rgb">
        /// The color's RGB values in an array.
        ///</param>
        public double CalcGrayScalePercentage(int[] rgb) {
            return CalcGrayScalePercentage(rgb[0], rgb[1], rgb[2]);
        }

        private void IsValidRgb(int[] rgb) {
            if (rgb.Length != 3) {
                throw new ArgumentException("The rgb array has not exactly 3 elements");
            }
            for (int i = 0; i < rgb.Length; i++) {
                if (rgb[i] > 255 || rgb[i] < 0) {
                    throw new ArgumentException($"The color {GetColorName(i)} value {rgb[i]} is out of bounds");
                }
            }
        }

        private string GetColorName(int index) {
            switch (index) {
                case 0:
                    return "red";
                case 1:
                    return "green";
                case 2:
                    return "blue";
                default:
                    throw new ArgumentException("Does not correspond to a color", "index");
            }
        }

    }
}
