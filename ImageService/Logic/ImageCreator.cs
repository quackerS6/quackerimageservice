﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;

namespace ImageService.Logic {
    public class ImageCreator {

        private readonly ColorCalculator _colorCalculator;
        private readonly string _path;
        private readonly Random _rng;
        private readonly int _imgSize;

        // Todo: make the text / geeese scale with this given image size
        public ImageCreator(string path, int imgSize = 192) {
            _path = Path.Combine(path, "Geese");
            _colorCalculator = new ColorCalculator();
            _rng = new Random();
            _imgSize = imgSize;
        }

        public Bitmap CreateNewProfileImage(string firstName = "placeholder", string lastName = " ") {
            // Todo: Split methods so there is one for goose without name and a letter when the name is supplied
            string imgText = GetImageText(firstName, lastName);

            var bmp = new Bitmap(_imgSize, _imgSize);
            var sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;

            var font = new Font("Arial", 72, FontStyle.Bold, GraphicsUnit.Pixel);
            var graphics = Graphics.FromImage(bmp);

            graphics.Clear(Color.Transparent);
            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
            using (Brush b = new SolidBrush(GenerateBackgroundColor())) {
                graphics.FillEllipse(b, new Rectangle(0, 0, _imgSize, _imgSize));
            }

            // The commented line below is what is drawn when the name needs to be on the image
            //graphics.DrawString(imgText, font, new SolidBrush(Color.WhiteSmoke), 95, 100, sf); 
            // Todo: Get the number of images in the folder instead of hardcoding (9)
            using (Image gooseOutline = Image.FromFile(Path.Combine(_path, $"Goose_Outline_{_rng.Next(1, 9)}.png"))) {
                int x = _imgSize / 2 - gooseOutline.Width / 2;
                int y = _imgSize / 2 - gooseOutline.Height / 2;
                graphics.DrawImage(gooseOutline, x, y);
            }
            graphics.Flush();
            return bmp;
        }


        private string GetImageText(string firstName, string lastName = " ") {
            string imgText = firstName[0].ToString().ToUpper();
            imgText += lastName[0].ToString().ToUpper();
            imgText.TrimEnd();
            return imgText;
        }

        private Color GenerateBackgroundColor() {
            int[] rgb = new int[3];
            do {
                for (int i = 0; i < rgb.Length; i++) {
                    rgb[i] = _rng.Next(1, 256);
                }
            } while (_colorCalculator.CalcGrayScalePercentage(rgb) > 50);

            return Color.FromArgb(rgb[0], rgb[1], rgb[2]);
        }

    }
}
