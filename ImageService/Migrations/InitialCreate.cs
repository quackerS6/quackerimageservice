﻿using FluentMigrator;

namespace ImageService.Migrations {
    [Migration(20210322)]
    public class InitialCreate : Migration {
        public override void Up() {
            Create.Table("images")
                .WithColumn("id").AsGuid().PrimaryKey().WithDefaultValue(SystemMethods.NewGuid)
                .WithColumn("url").AsString().NotNullable().Unique()
                .WithColumn("delete-id").AsString().NotNullable().Unique()
                .WithColumn("user-id").AsGuid().NotNullable().Unique();
        }

        public override void Down() {
            Delete.Table("images");
        }
    }
}
