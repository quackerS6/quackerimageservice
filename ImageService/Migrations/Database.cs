﻿using Dapper;
using System.Data.SqlClient;
using System.Linq;

namespace ImageService.Migrations {
    public static class Database {
        public static void EnsureCreated(string connectionString, string dbName) {
            var parameters = new DynamicParameters();
            parameters.Add("name", dbName);
            connectionString = CreateMigrationConnectionString(connectionString, dbName);
            using var connection = new SqlConnection(connectionString);
            var records = connection.Query("SELECT * FROM sys.databases WHERE name = @name", parameters);
            if (!records.Any()) {
                connection.Execute($"CREATE DATABASE {dbName}");
            }
        }

        public static string CreateMigrationConnectionString(string fullConnectionString, string dbName) {
            return fullConnectionString.Replace($"Database={dbName};", "");
        }
    }
}
