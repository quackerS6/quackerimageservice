﻿using System.IO;
using System.Threading.Tasks;

namespace ImageService.Imgur {
    public interface IImageApi {
        Task<ImageResponse> UploadImageToFolder(MemoryStream image);
        Task<ImageResponse> UploadImage(MemoryStream image);
        Task<bool> MoveImageToFolder(string imageId);
    }
}
