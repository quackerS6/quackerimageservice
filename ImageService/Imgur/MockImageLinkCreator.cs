﻿using System;

namespace ImageService.Imgur {
    public class MockImageLinkCreator {
        private readonly string _baseUrl = "https://via.placeholder.com/150";
        private readonly Random _random;
        private readonly string[] _imageTexts = new string[] { "Quacker", "Goose", "Untitled", "MyProfile" };
        //"https://via.placeholder.com/150/2226E5/FFFFFF?text=Quacker",

        public MockImageLinkCreator() {
            _random = new Random();
        }

        public string CreateImageLink() {
            return $"{_baseUrl}/{GetRandomColor()}/{GetRandomColor()}?text={GetRandomText()}";
        }

        private string GetRandomColor() {
            return string.Format("#{0:X6}", _random.Next(0x1000000)).Substring(1);
        }

        private string GetRandomText() {
            return _imageTexts[_random.Next(_imageTexts.Length)];
        }
    }
}
