﻿namespace ImageService.Imgur {
    public class ImgurData {
        public string ClientId { get; private set; }
        public string Token { get; private set; }
        public string RefreshToken { get; private set; }
        public string AlbumHash { get; private set; }
        public string AlbumDeleteHash { get; private set; }

        private string _baseUrl;

        public ImgurData(string clientId, string token, string refreshToken, string albumHash, string albumDeleteHash, string baseUrl) {
            ClientId = clientId;
            Token = token;
            RefreshToken = refreshToken;
            AlbumHash = albumHash;
            AlbumDeleteHash = albumDeleteHash;
            _baseUrl = baseUrl;
        }

        public string MoveToAlbumAuthedUrl {
            get {
                return $"{_baseUrl}/album/{AlbumHash}/add";
            }
        }

        public string MoveToAlbumUnauthedUrl {
            get {
                return $"{_baseUrl}/album/{AlbumDeleteHash}/add";
            }
        }

        public string UploadImageUrl {
            get {
                return $"{_baseUrl}/upload";
            }
        }

        public string DeleteImageUrl {
            get {
                return $"{_baseUrl}/image";
            }
        }


    }
}
