﻿using Microsoft.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace ImageService.Imgur {
    public class ImgurApi : IImageApi {

        private readonly ImgurData _data;
        private readonly IHttpClientFactory _httpClientFactory;

        public ImgurApi(ImgurData data, IHttpClientFactory httpClientFactory) {
            _data = data;
            _httpClientFactory = httpClientFactory;
        }

        public async Task<bool> MoveImageToFolder(string deleteHash) {
            // Todo: Make switching between Authed and unauthed requests easier
            using (var formData = new MultipartFormDataContent()) {
                formData.Add(new StringContent(deleteHash), "deletehashes[]");
                HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Post, _data.MoveToAlbumUnauthedUrl);
                httpRequest.Headers.Add(HeaderNames.Authorization, _data.Token);
                httpRequest.Content = formData;
                var response = await _httpClientFactory.CreateClient().SendAsync(httpRequest);
                return response.IsSuccessStatusCode;
            }
        }

        public async Task<ImageResponse> UploadImage(MemoryStream image) {
            string url = _data.UploadImageUrl;
            ImageResponse imgResponse = new ImageResponse();

            using (var content = new StreamContent(image)) {
                using (var formData = new MultipartFormDataContent()) {
                    string name = Guid.NewGuid() + ".png";
                    formData.Add(content, "image", name);
                    HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Post, url);
                    httpRequest.Headers.Add(HeaderNames.Authorization, _data.Token);
                    httpRequest.Content = formData;
                    var response = await _httpClientFactory.CreateClient().SendAsync(httpRequest);
                    string jsonString = await response.Content.ReadAsStringAsync();
                    JObject json = JObject.Parse(jsonString);
                    imgResponse.ImageId = json.SelectToken("data.id").ToString();
                    imgResponse.ImageDeleteHash = json.SelectToken("data.deletehash").ToString();
                    imgResponse.Url = json.SelectToken("data.link").ToString();
                }
            }
            return imgResponse;
        }

        public async Task<ImageResponse> UploadImageToFolder(MemoryStream image) {
            ImageResponse imgResponse = await UploadImage(image);
            imgResponse.DidMoveToFolder = await MoveImageToFolder(imgResponse.ImageDeleteHash);
            return imgResponse;
        }
    }
}
