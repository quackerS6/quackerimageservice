﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace ImageService.Imgur {
    public class DevelopImageApi : IImageApi {

        private ImageResponse _imgRepsonse;
        private readonly MockImageLinkCreator _linkCreator;

        public DevelopImageApi() {
            _linkCreator = new MockImageLinkCreator();
        }

        public async Task<bool> MoveImageToFolder(string _) {
            return await Task.FromResult(true);
        }

        public async Task<ImageResponse> UploadImage(MemoryStream _) {
            return await Task.FromResult(GetImageResponse());
        }

        public async Task<ImageResponse> UploadImageToFolder(MemoryStream _) {
            return await Task.FromResult(GetImageResponse());
        }

        public ImageResponse GetImageResponse() {
            return new ImageResponse() {
                ImageDeleteHash = Guid.NewGuid().ToString(),
                ImageId = Guid.NewGuid().ToString(),
                Url = _linkCreator.CreateImageLink(),
                DidMoveToFolder = true
            };
        }
    }
}
