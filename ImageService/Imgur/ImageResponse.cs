﻿namespace ImageService.Imgur {
    public class ImageResponse {
        public string ImageId { get; set; }
        public string ImageDeleteHash { get; set; }
        public string Url { get; set; }
        public bool DidMoveToFolder { get; set; } = false;
    }
}
