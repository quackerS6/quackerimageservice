﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;

namespace ImageService.Utils {
    public static class StartupExtentions {

        private static readonly string _imgur = "Imgur";

        public static string GetImgurSectionConfig(this IConfiguration config, string key, bool authSection) {
            string section = authSection ? "Authed" : "Unauthed";
            return config.GetSection(_imgur)[$"{section}:{key}"];
        }

        public static string GetImgurBaseConfig(this IConfiguration config, string key) {
            return config.GetSection(_imgur)[key];
        }

        public static IConfigurationBuilder SetDevSettingsIfNotInDocker(this IConfigurationBuilder builder, IWebHostEnvironment env) {
            if (!env.IsInDocker() && env.IsDevelopment()) {
                builder.AddJsonFile("appsettings.NoDockerDev.json");
            }
            return builder;
        }

        public static bool IsInDocker(this IWebHostEnvironment env) {
            return bool.Parse(Environment.GetEnvironmentVariable("DOTNET_RUNNING_IN_CONTAINER") ?? bool.FalseString);
        }
    }
}
