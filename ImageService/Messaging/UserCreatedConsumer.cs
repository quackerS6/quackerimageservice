﻿using ImageService.DAL;
using ImageService.Imgur;
using ImageService.Logic;
using MassTransit;
using Microsoft.AspNetCore.Hosting;
using Shared.Messages;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;

namespace ImageService.Messaging {
    public class UserCreatedConsumer : IConsumer<UserCreatedMessage> {

        private readonly IImageDal _db;
        private readonly IPublishEndpoint _broker;
        private readonly IImageApi _imageApi;
        private readonly ImageCreator _imageCreator;

        public UserCreatedConsumer(IImageDal db, IPublishEndpoint broker, IImageApi imageApi, IWebHostEnvironment env) {
            _db = db;
            _broker = broker;
            _imageApi = imageApi;
            _imageCreator = new ImageCreator(env.ContentRootPath);
        }

        public async Task Consume(ConsumeContext<UserCreatedMessage> context) {
            ImageResponse response;
            using (var ms = new MemoryStream()) {
                Bitmap bmp = _imageCreator.CreateNewProfileImage();
                bmp.Save(ms, ImageFormat.Png);
                ms.Seek(0, SeekOrigin.Begin);
                response = await _imageApi.UploadImageToFolder(ms);
                // Save to database
                var entity = new ImageEntity() {
                    DeleteId = response.ImageDeleteHash,
                    Url = response.Url,
                    UserId = context.Message.Id
                };

                await _db.Add(entity);
            }

            // Send to rabbit MQ
            await _broker.Publish(new ImageUploadMessage() {
                Url = response.Url,
                UserId = context.Message.Id
            });

        }

    }
}

