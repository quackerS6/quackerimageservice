﻿using Dapper;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace ImageService.DAL {
    public class ImageDal : IImageDal {

        private readonly string _connectionString;
        private readonly string _sqlAdd = "INSERT INTO images (url, [delete-id], [user-id]) VALUES (@Url, @DeleteId, @UserId)";
        // Could change in the future if other images than profile are stored
        private readonly string _sqlUpdateByUser = "UPDATE images SET url = @Url, [delete-id] = @DeleteId WHERE [user-id] = @UserId";
        private readonly string _sqlDeleteByUser = "DELETE FROM images WHERE [user-id] = @userId";
        private readonly string _sqlGetImageByUser = "DELETE FROM images WHERE [user-id] = @userId";

        public ImageDal(string connectionString) {
            _connectionString = connectionString;
        }

        public async Task Add(ImageEntity image) {
            using (var connection = new SqlConnection(_connectionString)) {
                await connection.ExecuteAsync(_sqlAdd, image);
            }
        }

        public async Task Update(ImageEntity image) {
            using (var connection = new SqlConnection(_connectionString)) {
                await connection.ExecuteAsync(_sqlUpdateByUser, image);
            }
        }

        public async Task DeleteByUserId(Guid userId) {
            using (var connection = new SqlConnection(_connectionString)) {
                await connection.ExecuteAsync(_sqlDeleteByUser, userId);
            }
        }

        public async Task<ImageEntity> GetByUserId(Guid userId) {
            using (var connection = new SqlConnection(_connectionString)) {
                return await connection.QueryFirstAsync<ImageEntity>(_sqlGetImageByUser, userId);
            }
        }

    }
}
