﻿using System;
using System.Threading.Tasks;

namespace ImageService.DAL {
    public interface IImageDal {
        Task Add(ImageEntity image);
        Task DeleteByUserId(Guid userId);
        Task<ImageEntity> GetByUserId(Guid userId);
        Task Update(ImageEntity image);
    }
}
