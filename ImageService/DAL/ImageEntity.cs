﻿using System;

namespace ImageService.DAL {
    public class ImageEntity {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string Url { get; set; }
        public string DeleteId { get; set; }

    }
}
