using FluentMigrator.Runner;
using GreenPipes;
using ImageService.DAL;
using ImageService.Imgur;
using ImageService.Messaging;
using ImageService.Migrations;
using ImageService.Utils;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace ImageService {
    public class Startup {

        public IConfiguration Configuration { get; }
        private readonly IWebHostEnvironment _env;
        private readonly string _dbName = "mediaDB";

        public Startup(IWebHostEnvironment env) {

            Configuration = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddJsonFile($"secrets/appsettings.{env.EnvironmentName}.json", optional: true)
            .SetDevSettingsIfNotInDocker(env)
            .AddEnvironmentVariables()
            .Build();

            _env = env;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddControllers();
            services.AddHttpClient();

            //Thread.Sleep(60000);

            // Rabbit MQ
            services.AddMassTransit(mt => {
                mt.AddConsumer<UserCreatedConsumer>();
                mt.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(config => {
                    config.UseHealthCheck(provider);
                    //config.UseRetry(retryConfig => {
                    //    retryConfig.Interval(8, TimeSpan.FromSeconds(20));
                    //});
                    config.Host(new Uri(Configuration.GetSection("RabbitMQ")["host"]), h => {
                        h.Username(Configuration.GetSection("RabbitMQ")["username"]);
                        h.Password(Configuration.GetSection("RabbitMQ")["password"]);
                    });
                    config.ReceiveEndpoint("mediaqueue", endpoint => {
                        endpoint.PrefetchCount = 16;
                        endpoint.UseMessageRetry(retry => retry.Interval(2, 100));
                        endpoint.ConfigureConsumer<UserCreatedConsumer>(provider);
                    });
                }));
            });

            services.AddMassTransitHostedService();



            bool useAuthenticatedApi = false;
            services.AddSingleton(new ImgurData(
                    Configuration.GetImgurBaseConfig("ClientId"),
                    Configuration.GetImgurSectionConfig("Token", useAuthenticatedApi),
                    Configuration.GetImgurSectionConfig("RefreshToken", true),
                    Configuration.GetImgurSectionConfig("AlbumHash", useAuthenticatedApi),
                    Configuration.GetImgurSectionConfig("AlbumDeleteHash", useAuthenticatedApi),
                    Configuration.GetImgurBaseConfig("BaseUrl")
            ));

            services.AddScoped<IImageDal>((_) => new ImageDal(Configuration.GetConnectionString(_dbName)));

            // Fluent Migrator
            services
                .AddFluentMigratorCore()
                .ConfigureRunner(migrationBuilder =>
                    migrationBuilder.AddSqlServer()
                      .WithGlobalConnectionString(Configuration.GetConnectionString(_dbName))
                      .ScanIn(typeof(InitialCreate).Assembly).For.Migrations()
                )
                .AddLogging(loggerBuilder =>
                    loggerBuilder.AddFluentMigratorConsole()
                );

            if (_env.IsDevelopment()) {
                services.AddScoped<IImageApi, ImgurApi>();
                //services.AddScoped<IImageApi, DevelopImageApi>();
            }
            else {
                services.AddScoped<IImageApi, ImgurApi>();
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app) {

            if (_env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });

            DoMigrations(app);
        }

        private void DoMigrations(IApplicationBuilder app) {

            Database.EnsureCreated(Configuration.GetConnectionString(_dbName), _dbName);
            // Migrate
            using (var scope = app.ApplicationServices.CreateScope()) {
                var runner = scope.ServiceProvider.GetRequiredService<IMigrationRunner>();
                runner.MigrateUp();
            }
        }
    }
}