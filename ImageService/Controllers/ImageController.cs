﻿using ImageService.DAL;
using ImageService.Imgur;
using ImageService.Logic;
using MassTransit;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;

namespace ImageService.Controllers {
    [ApiController]
    [Route("[controller]")]
    public class ImageController : ControllerBase {

        // private readonly Tuple<string, string> _header = Tuple.Create("Authorization", "Bearer d7300392c4dbf322cac6ae6b089b01a01af69b9d");
        //private static readonly HttpClient _client = new HttpClient();
        private readonly ImageCreator _imgCreator;
        private readonly IImageApi _imageApi;
        private readonly IImageDal _db;
        private readonly IBus _broker;

        public ImageController(IImageApi imageApi, IWebHostEnvironment env, IImageDal db, IBus broker) {
            _imageApi = imageApi;
            _imgCreator = new ImageCreator(env.ContentRootPath);
            _db = db;
            _broker = broker;
        }

        [HttpGet("generate")]
        public async Task<IActionResult> Image([FromQuery] string fName = "X", [FromQuery] string lName = " ") {
            using (var ms = new MemoryStream()) {
                Bitmap bmp = _imgCreator.CreateNewProfileImage(fName, lName);
                bmp.Save(ms, ImageFormat.Png);
                ms.Seek(0, SeekOrigin.Begin);
                return File(ms.ToArray(), "image/png", "Profile.png");
            }
        }

        [HttpPost]
        public async Task<IActionResult> UserImageUpload(/*[FromBody] ImageEntity image*/) {
            throw new NotImplementedException();
        }

    }
}
