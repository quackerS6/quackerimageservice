﻿using System;

namespace Shared.Messages {
    public class UserCreatedMessage {
        public Guid Id { get; set; }
    }
}
