﻿using Microsoft.AspNetCore.Http;
using System;

namespace ImageService.Models {
    public class UserImageUpload {
        public IFormFile UploadedImage { get; set; }
        public Guid UserId { get; set; }
    }
}
