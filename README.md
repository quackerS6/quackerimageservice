# QuackerImageService

Generates profile images for new users, and puts uploaded images on an external image service provider. (Imgur at the time of writing).
- A microservice for the quacker project